
class Tests{

    palidromo(cadena) {
	
    var palabra=cadena.toLowerCase();
 
	// eliminamos los espacios en blanco
	palabra=palabra.replace(/ /g, "");
 
	for (var i=0;i<palabra.length;i++){
		if(palabra[i]!=palabra[palabra.length-i-1]){
			return false;
		}
	}
	return true;
}

getpalindromo(){
    if(palidromo()) {
        return"Esto es palíndromo";
    }else{
        return "Esto no es palíndromo";
    }
}

}

module.exports = Tests;