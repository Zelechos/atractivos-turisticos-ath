'use strict'

const {it, expect} = require("@jest/globals");
const palindromo = require("./palindromo");

describe('palindromo ==> tests !!!', ()=>{
    const pal = new palindromo();

    it('Verificar si es palidromo para el cadena solos', ()=>{
        const response = pal.palidromo("solos");
        expect(response).toBe(true);
    });

    it('Verificar si es palidromo para la cadena nunca', ()=>{
        const response = pal.palidromo("nunca");
        expect(response).toBe(false);
    });

    it('Verificar si es capicua el numero 909', ()=>{
        const response = pal.palidromo("909");
        expect(response).toBe(true);
    });

    it('Verificar si es capicua el numero 90459', ()=>{
        const response = pal.palidromo("90459");
        expect(response).toBe(false);
    });

    
});